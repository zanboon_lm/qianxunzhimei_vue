import request from '@/utils/request'

export function add(token, data) {
  return request({
    url: 'shop_admin_auth/shop_role/add',
    method: 'post',
    params: { token },
    data
  })
}
export function edit(token, data) {
  return request({
    url: 'shop_admin_auth/shop_role/edit',
    method: 'post',
    params: { token },
    data
  })
}
export function del(token, data) {
  return request({
    url: 'shop_admin_auth/shop_role/delete',
    method: 'post',
    params: { token },
    data
  })
}
export function info(token, data) {
  return request({
    url: 'shop_admin_auth/shop_role/info',
    method: 'post',
    params: { token },
    data
  })
}
export function getList(token, data) {
  return request({
    url: 'shop_admin_auth/shop_role/get_list',
    method: 'post',
    params: { token },
    data
  })
}
