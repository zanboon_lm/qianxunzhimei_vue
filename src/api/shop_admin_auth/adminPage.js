import request from '@/utils/request'

export function addPage(token, data) {
  return request({
    url: 'shop_admin_auth/admin_page/add',
    method: 'post',
    params: { token },
    data
  })
}
export function del(token, data) {
  return request({
    url: 'shop_admin_auth/admin_page/delete',
    method: 'post',
    params: { token },
    data
  })
}
export function edit(token, data) {
  return request({
    url: 'shop_admin_auth/admin_page/edit',
    method: 'post',
    params: { token },
    data
  })
}
export function getPage(token) {
  return request({
    url: 'shop_admin_auth/admin_page/get_page_tree',
    method: 'post',
    params: { token }
  })
}
export function addRole(token, data) {
  return request({
    url: 'shop_admin_auth/shop_admin_page_role/add',
    method: 'post',
    params: { token },
    data
  })
}
export function moveTo(token, data) {
  return request({
    url: 'shop_admin_auth/admin_page/move_to',
    method: 'post',
    params: { token },
    data
  })
}

