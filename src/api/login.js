import request from '@/utils/request'

export function loginByUsername(username, password) {
  const data = {
    username: username,
    password: password
  }
  return request({
    url: 'login/login/index',
    method: 'get',
    params: data
  })
}

export function logout() {
  return request({
    url: '/login/logout',
    method: 'post'
  })
}

export function getUserInfo(token) {
  return request({
    url: 'user/admin_user/info_by_token',
    method: 'get',
    params: { token }
  })
}

export function getRouter(token) {
  return request({
    url: 'shop_admin_auth/admin_page/get_page_tree',
    method: 'post',
    params: { token }
  })
}
