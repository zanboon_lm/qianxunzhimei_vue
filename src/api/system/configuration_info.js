import request from '@/utils/request'

export function add(token, data) {
  return request({
    url: 'system/configuration_info/add',
    method: 'post',
    params: { token },
    data
  })
}
export function edit(token, data) {
  return request({
    url: 'system/configuration_info/edit',
    method: 'post',
    params: { token },
    data
  })
}
export function del(token, data) {
  return request({
    url: 'system/configuration_info/delete',
    method: 'post',
    params: { token },
    data
  })
}
export function info(token, data) {
  return request({
    url: 'system/configuration_info/info',
    method: 'post',
    params: { token },
    data
  })
}
export function getList(token, data) {
  return request({
    url: 'system/configuration_info/get_list',
    method: 'post',
    params: { token },
    data
  })
}
export function save(token, data) {
  return request({
    url: 'system/configuration_info/save',
    method: 'post',
    params: { token },
    data
  })
}
