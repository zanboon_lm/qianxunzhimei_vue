import request from '@/utils/request'

export function add(token, data) {
  return request({
    url: 'merchant/merchant_audit_data/add',
    method: 'post',
    params: { token },
    data
  })
}
export function edit(token, data) {
  return request({
    url: 'merchant/merchant_audit_data/edit',
    method: 'post',
    params: { token },
    data
  })
}
export function del(token, data) {
  return request({
    url: 'merchant/merchant_audit_data/delete',
    method: 'post',
    params: { token },
    data
  })
}
export function info(token, data) {
  return request({
    url: 'merchant/merchant_audit_data/info',
    method: 'post',
    params: { token },
    data
  })
}
export function getAuditList(token, data) {
  return request({
    url: 'merchant/merchant_audit_data/get_list',
    method: 'post',
    params: { token },
    data
  })
}
