import request from '@/utils/request'

export function add(token, data) {
  return request({
    url: 'merchant/merchant/add',
    method: 'post',
    params: { token },
    data
  })
}
export function edit(token, data) {
  return request({
    url: 'merchant/merchant/edit',
    method: 'post',
    params: { token },
    data
  })
}
export function del(token, data) {
  return request({
    url: 'merchant/merchant/delete',
    method: 'post',
    params: { token },
    data
  })
}
export function info(token, data) {
  return request({
    url: 'merchant/merchant/info',
    method: 'post',
    params: { token },
    data
  })
}
export function getList(token, data) {
  return request({
    url: 'merchant/merchant/get_merchant_list',
    method: 'post',
    params: { token },
    data
  })
}
export function invoke(token, data) {
  return request({
    url: 'merchant/merchant/invoke',
    method: 'post',
    params: { token },
    data
  })
}
export function forbidden(token, data) {
  return request({
    url: 'merchant/merchant/forbidden',
    method: 'post',
    params: { token },
    data
  })
}
