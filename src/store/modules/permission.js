import { constantRouterMap } from '@/router'
import { getRouter } from '@/api/login'
/* Layout */
import Layout from '@/views/layout/Layout'
import store from '@/store'
/**
 * 通过meta.role判断是否与当前用户权限匹配
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.indexOf(role) >= 0)
  } else {
    return true
  }
}

/**
 * 递归过滤异步路由表，返回符合用户角色权限的路由表
 * @param asyncRouterMap
 * @param roles
 */
function filterAsyncRouter(asyncRouterMap, roles) {
  const accessedRouters = asyncRouterMap.filter(route => {
    if (route.component === 'Layout') {
      route.component = Layout
    } else if (typeof route.component === 'string' || route.component instanceof String) {
      var str = route.component
      route.component = () => import(`@/views/${str}`)
    }
    // console.log(route.meta.roles)
    if (hasPermission(roles, route)) {
      if (route.children && route.children.length) {
        route.children = filterAsyncRouter(route.children, roles)
      }
      return true
    }
    return false
  })
  return accessedRouters
}

const permission = {
  state: {
    routers: constantRouterMap,
    addRouters: []
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
      state.routers = constantRouterMap.concat(routers)
    }
  },
  actions: {
    GenerateRoutes({ commit }, data) {
      // console.log('根据roles权限生成可访问的路由表')
      return getRouter(store.getters.token).then(response => {
        return response.data.data
      }).then(asyncRouterMap => {
        // console.log(asyncRouterMap)
        const { roles } = data
        let accessedRouters
        if (asyncRouterMap) {
          accessedRouters = filterAsyncRouter(asyncRouterMap, roles)
        }
        commit('SET_ROUTERS', accessedRouters)
      })
    }
  }
}

export default permission
